# codetowp-rust
Rust Learning. Rewrite https://magic.ryza.moe/ryza/codetowp in Rust

### Details
You may refer to my blog post [here](https://ryza.moe/2019/10/rust-learning-from-zero-9/).

And it's associated with a previous post [here](https://ryza.moe/2019/08/rewrite-the-styled-code-in-html-generated-by-apple-to-wordpress-compatible-html/).

### Screenshot
![screenshot](screenshot.png)
